<?php

function autorais_ajax_posts() {
	$new_query = new WP_Query( 
		array( 
			'posts_per_page' => -1, 
			'post_type' => 'tatuagens',
			'order'	=>	'ASC',
			'tax_query' => array(
		        array(
		            'taxonomy' => 'categoria',
		            'field'    => 'slug',
		            'terms'    => 'autorais',
		        ),
		    ),
		) 
	);
    if( $new_query->have_posts() ) :
        while( $new_query->have_posts() ): $new_query->the_post(); ?>
			<div class="grid-item grid-item--width2">
				<?php if ( has_post_thumbnail() ) { ?>
					<img src="<?php the_post_thumbnail_url(); ?>">
				<?php } ?>
			</div>
        <?php endwhile;
        wp_reset_postdata();  
    endif;

    exit;
}
add_action('wp_ajax_autorais_ajax_posts', 'autorais_ajax_posts');
add_action('wp_ajax_nopriv_autorais_ajax_posts', 'autorais_ajax_posts');


function variados_ajax_posts() {
	$new_query = new WP_Query( 
		array( 
			'posts_per_page' => -1, 
			'post_type' => 'tatuagens',
			'order'	=>	'ASC',
			'tax_query' => array(
		        array(
		            'taxonomy' => 'categoria',
		            'field'    => 'slug',
		            'terms'    => 'variados',
		        ),
		    ),
		) 
	);
    if( $new_query->have_posts() ) :
        while( $new_query->have_posts() ): $new_query->the_post(); ?>
			<div class="grid-item grid-item--width2">
				<?php if ( has_post_thumbnail() ) { ?>
					<img src="<?php the_post_thumbnail_url(); ?>">
				<?php } ?>
			</div>
        <?php endwhile;
        wp_reset_postdata();  
    endif;

    exit;
}
add_action('wp_ajax_variados_ajax_posts', 'variados_ajax_posts');
add_action('wp_ajax_nopriv_variados_ajax_posts', 'variados_ajax_posts');

?>