	<div class="scrolltop">
		<a href="#topo">
			<img class="img-center" src="<?php echo bloginfo("template_url"); ?>/img/scrolltop.png">
		</a>
	</div>
	<footer class="hidden-lg">
		<img class="estrela-footer" src="<?php echo bloginfo("template_url"); ?>/img/estrela-footer.png">
		<div class="container">
			<div class="col-xs-5">
				<img class="logo-footer" src="<?php echo bloginfo("template_url"); ?>/img/logo-footer.png">
			</div>
			<div class="col-xs-2"></div>
			<div class="col-xs-5 redes-sociais">
				<ul>
					<li>
						<a href="https://instagram.com/" target="_blank">
							<img src="<?php echo bloginfo("template_url"); ?>/img/instagram.png"> Instagram
						</a>
					</li>
					<li>
						<a href="mailto:" target="_blank">
							<img src="<?php echo bloginfo("template_url"); ?>/img/email.png"> Email
						</a>
					</li>
				</ul>
			</div>
		</div>
	</footer>

	<footer class="footer-desktop hidden-xs">
		<img class="estrela-footer" src="<?php echo bloginfo("template_url"); ?>/img/estrela-footer.png">
		<div class="container-fluid">
			<div class="col-sm-2">
				<img class="logo-footer" src="<?php echo bloginfo("template_url"); ?>/img/logo-footer-desktop.png">
			</div>
			<div class="col-sm-8">
				<ul class="menu-footer">
					<li><a href="<?php echo site_url(); ?>/tatuagens">Conheça meu trabalho</a></li>
					<li><a class="link-orcamento" href="<?php echo site_url(); ?>/orcamento">Orçamento</a></li>
					<li><a class="link-sobre" href="<?php echo site_url(); ?>/sobre">Sobre mim</a></li>
					<li><a class="link-cuidados" href="<?php echo site_url(); ?>/cuidados">Cuidados com sua tattoo</a></li>
					<li><a href="<?php echo site_url(); ?>/contato">contato</a></li>
					<li><a href="https://lawmelo.com/loja" target="_blank">Loja</a></li>
				</ul>
			</div>
			<div class="col-sm-2">
				<ul class="contato-footer">
					<li>
						<a href="https://instagram.com/" target="_blank">
							<img src="<?php echo bloginfo("template_url"); ?>/img/instagram.png"> Instagram
						</a>
					</li>
					<li>
						<a href="mailto:" target="_blank">
							<img src="<?php echo bloginfo("template_url"); ?>/img/email.png"> Email
						</a>
					</li>
				</ul>
			</div>
		</div>
	</footer>

	<div class="copyright">
		<p>Design por Pedro Savio e desenvolvimento por Thiago Chui</p>
	</div>	

	<script type="text/javascript" src="<?php echo bloginfo("template_url"); ?>/js/main.js"></script>
	<?php wp_footer(); ?>
</body>
</html>