<?php


require_once get_template_directory() . '/ajax-tatuagens.php';


function admin_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed at WordPress.org. See: https://translate.wordpress.org/projects/wp-themes/admin
	 * If you're building a theme based on Twenty Seventeen, use a find and replace
	 * to change 'admin' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'admin' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	add_image_size( 'admin-featured-image', 2000, 1200, true );

	add_image_size( 'admin-thumbnail-avatar', 100, 100, true );

	// Set the default content width.
	$GLOBALS['content_width'] = 525;

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'top'    => __( 'Top Menu', 'admin' ),
		'social' => __( 'Social Links Menu', 'admin' ),
	) );
}
add_action( 'after_setup_theme', 'admin_setup' );

$labels = array(
	'name'                => "Slider Principal",
	'singular_name'       => "Slider Principal",
	'menu_name'           => "Slider Principal",
	'name_admin_bar'      => "Slider Principal"
);

$args = array(
	'labels'              => $labels,
	'supports'            => array('title', 'thumbnail'),
	'hierarchical'        => false,
	'public'              => true,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'menu_icon'           => 'dashicons-format-image',
	'show_in_admin_bar'   => true,
	'show_in_nav_menus'   => true,
	'can_export'          => true,
	'has_archive'         => false,
	'exclude_from_search' => true,
	'publicly_queryable'  => true,
	'capability_type'     => 'page',
);
register_post_type( 'slider', $args );

$labels = array(
	'name'                => "Tatuagens",
	'singular_name'       => "Tatuagens",
	'menu_name'           => "Tatuagens",
	'name_admin_bar'      => "Tatuagens"
);

$args = array(
	'labels'              => $labels,
	'supports'            => array('title', 'editor', 'thumbnail'),
	'hierarchical'        => false,
	'public'              => true,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'menu_icon'           => 'dashicons-heart',
	'show_in_admin_bar'   => true,
	'show_in_nav_menus'   => true,
	'can_export'          => true,
	'has_archive'         => false,
	'exclude_from_search' => true,
	'publicly_queryable'  => true,
	'capability_type'     => 'page',
);

register_post_type( 'tatuagens', $args );
add_action( 'init', 'categoria_tatuagens' );
function categoria_tatuagens() {
	register_taxonomy(
		'categoria',
		'tatuagens',
		array(
			'label' => __( 'Categoria' ),
			'rewrite' => array( 'slug' => 'categoria' ),
			'hierarchical' => true,
		)
	);
}


show_admin_bar(false);

?>