<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<title><?php bloginfo('name'); ?></title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link type="text/css" rel="stylesheet" href="<?php echo bloginfo("template_url"); ?>/css/main.css">
	<link type="text/css" rel="stylesheet" href="<?php echo bloginfo("template_url"); ?>/css/font.css">
	<link rel="stylesheet" href="<?php echo bloginfo("template_url"); ?>/css/bootstrap.min.css"> 
	<link rel="stylesheet" href="<?php echo bloginfo("template_url"); ?>/css/animate.min.css">
	<link rel="stylesheet" href="<?php echo bloginfo("template_url"); ?>/css/font-awesome.css">
	<link rel="stylesheet" href="<?php echo bloginfo("template_url"); ?>/css/owl.carousel.css">
	<link rel='shortcut icon' type='image/x-icon' href='<?php echo bloginfo("template_url"); ?>/img/favicon.png' />

	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">
	
	<script src="<?php echo bloginfo("template_url"); ?>/js/jquery-1.11.3.min.js"></script>
	<script src="<?php echo bloginfo("template_url"); ?>/js/bootstrap.min.js"></script>
	<script src="<?php echo bloginfo("template_url"); ?>/js/owl.carousel.js"></script>
	<script src="<?php echo bloginfo("template_url"); ?>/js/smooth-scroll.js"></script>	
	<script src="<?php echo bloginfo("template_url"); ?>/js/wow.js"></script>
	<script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js"></script>

	<?php wp_head(); ?>
</head>
<body>

	<header class="header clear hidden-lg" role="banner">
		<img class="menu-icon" src="<?php echo bloginfo("template_url"); ?>/img/menu-icon.png">
		<div class="menu">
			<ul>
				<li><a href="<?php echo site_url(); ?>">início</a></li>
				<li><a href="<?php echo site_url(); ?>/tatuagens">tatuagens</a></li>
				<li><a class="link-sobre" href="<?php echo site_url(); ?>/#sobre">sobre mim</a></li>
				<li><a class="link-orcamento" href="<?php echo site_url(); ?>/#orcamento">orçamento</a></li>
				<li><a class="link-cuidados" href="<?php echo site_url(); ?>/#cuidados">cuidados</a></li>
				<li><a href="https://lawmelo.com/loja" target="_blank">loja</a></li>
				<li><a href="<?php echo site_url(); ?>/contato">contato</a></li>
			</ul>
		</div>
	</header>
	<div id="topo" class="hidden-lg">
		<div class="container">
			<a href="<?php echo site_url(); ?>">
				<img class="img-center logo" src="<?php echo bloginfo("template_url"); ?>/img/logo.png">
			</a>
		</div>
	</div>

	<nav class="navbar navbar-default hidden-xs">
		  <div class="container-fluid">
		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		    	<div class="col-sm-2">
			    	<a href="<?php echo site_url(); ?>">
			    		<img class="logo-header hidden-xs" src="<?php echo bloginfo("template_url"); ?>/img/logo.png">
			    	</a>
			    </div>
		    	<div class="col-sm-8">
			      <ul class="nav navbar-nav">
			        <li><a href="<?php echo site_url(); ?>/tatuagens">tatuagens</a></li>
					<li><a class="link-orcamento" href="<?php echo site_url(); ?>/orcamento">orçamento</a></li>
					<li><a class="link-sobre" href="<?php echo site_url(); ?>/sobre">sobre mim</a></li>
					<li><a class="link-cuidados" href="<?php echo site_url(); ?>/cuidados">cuidados</a></li>
					<li><a href="<?php echo site_url(); ?>/contato">contato</a></li>
			      </ul>
		  		</div>
		  		<div class="col-sm-2 btn-loja">
		  			<a href="https://lawmelo.com/loja" target="_blank">loja</a>
		  		</div>
		  	</div>
		</nav>
	




