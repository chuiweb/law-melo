<?php get_header(); ?>
	
	<div class="mobile hidden-lg">
		<div class="topo">
			<div class="container">
				<img class="tattoo-autoral" src="<?php echo bloginfo("template_url"); ?>/img/tatuagem-autoral.png">
				<img class="estrela-topo" src="<?php echo bloginfo("template_url"); ?>/img/estrela.png">
				<div class="col-xs-1"></div>
				<div class="col-xs-10">
					<div class="owl-carousel" id="slide-topo">
						<?php
						    $args = array(
						        'post_type' => 'slider',
						        'posts_per_page' => -1
						    );

						    $post_query = new WP_Query($args);
							if($post_query->have_posts() ) { while($post_query->have_posts() ) { $post_query->the_post(); 
						?>
						<div class="item">
							<?php if ( has_post_thumbnail() ) { ?>
								<img src="<?php the_post_thumbnail_url(); ?>">
							<?php } ?>
						</div>
						<?php } } wp_reset_postdata(); ?>
					</div>
				</div>
			</div>
		</div>

		<div class="botao botao-topo">
			<a href="<?php echo site_url(); ?>/tatuagens">Tatuagens</a>
		</div>

		<div class="texto-autoral">
			<div class="container">
				<p>Eu trabalho somente com <strong>desenhos autorais</strong> e criações especiais: desenhos prontos com criação totalmente minha; ou criações com base na sua ideia e referências, e aí desenvolvo uma ilustração exclusiva. Assim você vai levar na pele uma tatuagem única e só sua ;)</p>
			</div>
		</div>

		<div class="sobre-mim" id="sobre">
			<div class="container">
				<img class="circle-law" src="<?php echo bloginfo("template_url"); ?>/img/circle-law.png">
				<img class="sobre-law" src="<?php echo bloginfo("template_url"); ?>/img/sobre-law.png">
				<img class="law-melo" src="<?php echo bloginfo("template_url"); ?>/img/law-melo.png">
			</div>
			<div class="botao botao-sobre-mim">
				<a class="expande" href="#sobre">Sobre mim</a>
			</div>
		</div>
		<div class="sobre-expande">
			<div class="container">
				<?php 
					$id=19; 
					$post = get_post($id); 
					$content = apply_filters('the_content', $post->post_content); 
					echo $content; 
				?>
				<div class="botao btn-conheca">
					<a href="#">Conheça o meu trabalho</a>
				</div>
			</div>
		</div>

		<div class="container">
			<h2 class="frase">sua tattoo feita com muito amor</h2>
		</div>

		<div class="agenda" id="orcamento">
			<div class="container">
				<h2>agenda</h2>
				<h5>aberta</h5>
				<p>Se você tem interesse em tatuar comigo, clique no link abaixo e preencha seus dados que logo eu respondo com o seu orçamento :) </p>
				<p>Você vai receber um email para que a gente converse um pouquinho sobre suas ideias!</p>

				<div class="botao">
					<a href="#">Orçamento</a>
				</div>

			</div>
		</div>

		<div class="cuidados" id="cuidados">
			<div class="container">
				<img class="cuidados-texto" src="<?php echo bloginfo("template_url"); ?>/img/cuidados.png">
				<img class="cuidados-tattoo" src="<?php echo bloginfo("template_url"); ?>/img/cuidados-tattoo.png">

			</div>
		</div>
		<div class="cuidados-descricao">
			<div class="container">
				<p>Tudo o que você precisa saber, antes, durante e depois de fazer uma tattoo <3</p>
			</div>
		</div>
		<div class="botao botao-cuidados">
			<a href="<?php echo site_url(); ?>/cuidados">Cuidados</a>
		</div>

		<div class="depoimentos">
			<div class="container">
				<h2>depoimentos</h2>
				<div class="owl-carousel" id="slide-depoimentos">
					<?php
					    //$args = array(
					        //'post_type' => 'depoimentos',
					        //'posts_per_page' => -1
					    //);

					    //$post_query = new WP_Query($args);
						//if($post_query->have_posts() ) { while($post_query->have_posts() ) { $post_query->the_post(); 
					?>
					<div class="item">
						<div class="texto-depoimento">
							<p>“Tatuar com a Law é sempre uma experiência incrível, além de seus desenhos serem simplesmente maravilhosos ela é extremamente cuidadosa e profissional. Me sinto muito acolhido ao tatuar com ela, recomendo muitooo <3”</p>
							<p><span>-</span> Pedro Savio</p>
						</div>
					</div>
					<div class="item">
						<div class="texto-depoimento">
							<p>“Tatuar com a Law é sempre uma experiência incrível, além de seus desenhos serem simplesmente maravilhosos ela é extremamente cuidadosa e profissional. Me sinto muito acolhido ao tatuar com ela, recomendo muitooo <3”</p>
							<p><span>-</span> Pedro Savio</p>
						</div>
					</div>
					<?php //} } wp_reset_postdata(); ?>
				</div>
			</div>
		</div>
	</div>

	<div class="desktop hidden-xs">
		<div class="topo">
			<div class="container">
				<h1>tatuagem<br> autoral</h1>
				<img class="estrela-topo" src="<?php echo bloginfo("template_url"); ?>/img/estrela.png">
				<div class="col-xs-1"></div>
				<div class="col-xs-10">
					<div class="owl-carousel" id="slide-topo-desktop">
						<?php
						    $args = array(
						        'post_type' => 'slider',
						        'posts_per_page' => -1
						    );

						    $post_query = new WP_Query($args);
							if($post_query->have_posts() ) { while($post_query->have_posts() ) { $post_query->the_post(); 
						?>
						<div class="item">
							<?php if ( has_post_thumbnail() ) { ?>
								<img src="<?php the_post_thumbnail_url(); ?>">
							<?php } ?>
						</div>
						<?php } } wp_reset_postdata(); ?>
					</div>
				</div>
			</div>
		</div>
			
		<div class="texto-autoral">
			<div class="container">
				<div class="col-sm-2"></div>
				<div class="col-sm-10">
					<p>Eu trabalho somente com <strong>desenhos autorais</strong> e criações especiais: desenhos prontos com criação totalmente minha; ou criações com base na sua ideia e referências, e aí desenvolvo uma ilustração exclusiva. Assim você vai levar na pele uma tatuagem única e só sua ;)</p>
				</div>
			</div>
		</div>

		<div class="container">
			<div class="botao botao-topo btn-conheca">
				<a href="<?php echo site_url(); ?>/tatuagens">Conheça o meu trabalho</a>
			</div>
		</div>

		<div class="container-fluid">
			<div class="sobre-mim" id="sobre">
				<img class="circle-law" src="<?php echo bloginfo("template_url"); ?>/img/circle-law.png">
				<img class="sobre-law" src="<?php echo bloginfo("template_url"); ?>/img/law-desktop.png">
				<img class="law-melo" src="<?php echo bloginfo("template_url"); ?>/img/law-melo.png">
				<div class="botao botao-sobre-mim">
					<a href="<?php echo site_url(); ?>/sobre">Sobre mim</a>
				</div>
			</div>
		</div>

		<div class="container">
			<h2 class="frase">sua tattoo feita com<br> muito amor</h2>
		</div>

		<div class="container-fluid">
			<div class="agenda row" id="orcamento">
				<div class="col-sm-6 col-img-agenda">
					<img src="<?php echo bloginfo("template_url"); ?>/img/agenda-aberta.png">
				</div>
				<div class="col-sm-6">
					<h2>agenda aberta</h2>
					<p>Se você tem interesse em tatuar comigo, clique no link abaixo e preencha seus dados que logo eu respondo com o seu orçamento :)</p>
					<br>
					<p>Você vai receber um email para que a gente converse um pouquinho sobre suas ideias!</p>
					<div class="botao">
						<a href="#">Orçamento</a>
					</div>
				</div>
			</div>
		</div>

		<div class="cuidados">
			<div class="container-fluid">
				<div class="col-sm-1"></div>
				<div class="col-sm-4">
					<h2>cuidados com<br> a sua tattoo</h2>
					<p>Tudo o que você precisa saber, antes,<br> durante e depois de fazer uma tattoo <3</p>
					<div class="botao">
						<a href="#">Cuidados com a sua tattoo</a>
					</div>
				</div>
				<div class="col-sm-7">
					<img src="<?php echo bloginfo("template_url"); ?>/img/img-cuidados-desktop.png">
				</div>
			</div>
		</div>

		<div class="depoimentos">
			<div class="container-fluid">
				<div class="col-sm-1"></div>
				<div class="col-sm-11">
					<h2>depoimentos</h2>
					<div class="owl-carousel" id="slide-depoimentos-desktop">
						<?php
						    //$args = array(
						        //'post_type' => 'depoimentos',
						        //'posts_per_page' => -1
						    //);

						    //$post_query = new WP_Query($args);
							//if($post_query->have_posts() ) { while($post_query->have_posts() ) { $post_query->the_post(); 
						?>
						<div class="item">
							<div class="texto-depoimento">
								<p>“Tatuar com a Law é sempre uma experiência incrível, além de seus desenhos serem simplesmente maravilhosos ela é extremamente cuidadosa e profissional. Me sinto muito acolhido ao tatuar com ela, recomendo muitooo <3”</p>
								<p><span>-</span> Pedro Savio</p>
							</div>
						</div>
						<div class="item">
							<div class="texto-depoimento">
								<p>“Tatuar com a Law é sempre uma experiência incrível, além de seus desenhos serem simplesmente maravilhosos ela é extremamente cuidadosa e profissional. Me sinto muito acolhido ao tatuar com ela, recomendo muitooo <3”</p>
								<p><span>-</span> Pedro Savio</p>
							</div>
						</div>
						<?php //} } wp_reset_postdata(); ?>
					</div>
				</div>
			</div>
		</div>

	</div>

<?php get_footer(); ?>