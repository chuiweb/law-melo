$(document).ready(function(){

	$("#slide-topo, #slide-topo-desktop").owlCarousel({
		nav : false,
		dots : true,
		items : 1,
		autoplay : true,
		loop : true,
  	});

  	$("#slide-depoimentos").owlCarousel({
		nav : false,
		dots : true,
		items : 1,
		autoplay : true,
		loop : true,
  	});

  	$("#slide-depoimentos-desktop").owlCarousel({
		nav : false,
		dots : true,
		items : 2,
		autoplay : true,
		loop : true,
		stagePadding : 50,
		margin : 50,
  	});

  	$('.grid').masonry({
	  itemSelector: '.grid-item',
	});

  	$(".expande").click(function(){
  		$(this).addClass("expande-open");
  		$(".sobre-mim").toggleClass("sobre-aberta");
  		$(".sobre-expande").toggleClass("sobre-expande-open");
  		$(".sobre-mim .law-melo, .sobre-mim .sobre-law, .sobre-mim .circle-law, .sobre-mim .flecha, .texto-autoral p, .botao-sobre-mim").toggleClass("change");
  		$(".topo, .botao-topo, .agenda, .cuidados, .botao-cuidados, .cuidados-descricao, h2.frase, .depoimentos").toggleClass("hide");
  	});
  	$(".expande-open").click(function(){
  		$('html, body').animate({
	        scrollTop: $("#sobre").offset().top
	    }, 2000);
  	});

  	$(".menu-icon").click(function(){
  		$(".menu").toggleClass("menu-open");
  	});

  	$(".link-sobre").click(function(){
  		$(".menu").removeClass("menu-open");
  		$('html, body').animate({
	        scrollTop: $("#sobre").offset().top
	    }, 2000);
  	});

  	$(".link-orcamento").click(function(){
  		$(".menu").removeClass("menu-open");
  		$('html, body').animate({
	        scrollTop: $("#orcamento").offset().top
	    }, 2000);
  	});

  	$(".link-cuidados").click(function(){
  		$(".menu").removeClass("menu-open");
  		$('html, body').animate({
	        scrollTop: $("#cuidados").offset().top
	    }, 2000);
  	});

  	$(".scrolltop").click(function(){
  		$('html, body').animate({
	        scrollTop: $("#topo").offset().top
	    }, 2000);
  	});

});