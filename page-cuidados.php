<?php get_header(); ?>
	<div class="page-cuidados" id="page-cuidados">
		<div class="container">
			<img class="cuidados-texto" src="<?php echo bloginfo("template_url"); ?>/img/cuidados.png">
			<img class="cuidados-tattoo" src="<?php echo bloginfo("template_url"); ?>/img/cuidados-tattoo.png">
		</div>
		<div class="container conteudo-cuidados">
			<?php 
				$id=5; 
				$post = get_post($id); 
				$content = apply_filters('the_content', $post->post_content); 
				echo $content; 
			?>
		</div>
	</div>
	<?php get_footer(); ?>