<?php get_header(); ?>

	<div class="page-tatuagens">
		<div class="container">
			<h2 class="titulo">tatuagens autorais</h2>

			<p>Eu trabalho somente com desenhos autorais e criações especiais: desenhos prontos com criação totalmente minha; ou criações com base na sua ideia e referências, e aí desenvolvo uma ilustração exclusiva. Assim você vai levar na pele uma tatuagem única e só sua ;) Conheça um pouquinho dos meus trabalhos já realizados.</p>

			<div class="filtros">
				<?php  get_template_part('taxonomias', 'tatuagens'); ?>
			</div>
		</div>
		<div class="container">
			<div class="posts-area grid" id="posts-area">
				<?php
				    $args = array(
				        'post_type' => 'tatuagens',
				        'posts_per_page' => -1,
				        'order'	=>	'ASC'
				    );

				    $post_query = new WP_Query($args);
					if($post_query->have_posts() ) { while($post_query->have_posts() ) { $post_query->the_post(); 
				?>
				<div class="grid-item grid-item--width2">
					<?php if ( has_post_thumbnail() ) { ?>
						<img src="<?php the_post_thumbnail_url(); ?>">
					<?php } ?>
				</div>
				<?php } } wp_reset_postdata(); ?>
			</div>
		</div>

		<div class="agenda" id="orcamento">
			<div class="container">
				<h2>agenda</h2>
				<h5>aberta</h5>
				<p>Se você tem interesse em tatuar comigo, clique no link abaixo e preencha seus dados que logo eu respondo com o seu orçamento :) </p>
				<p>Você vai receber um email para que a gente converse um pouquinho sobre suas ideias!</p>

				<div class="botao">
					<a href="#">Orçamento</a>
				</div>

			</div>
		</div>

	</div>
	
<?php get_footer(); ?>