<?php 
	$tax_produtos = get_terms('categoria');
	foreach ($tax_produtos as $taxonomia) {
		$args = array(
	        'post_type' => 'tatuagens',
	        'posts_per_page' => -1,
	        'order'	=>	'ASC',
	        'tax_query' => array(
		        array(
		            'taxonomy' => 'categoria',
		            'field'    => 'slug',
		            'terms'    => $taxonomia->slug,
		        ),
		    ),
	    );
	    if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	    	<div class="col-xs-6 col-tatuagens">
				<a href="#" id="<?php echo $taxonomia->slug; ?>" class="link-<?php echo $taxonomia->slug; ?> links-scroll">
					<h5><?php echo $taxonomia->name; ?></h5>
				</a>
			</div>
			<script type="text/javascript">

			 	jQuery(".link-<?php echo $taxonomia->slug; ?>").click(function(e){
			 		event.preventDefault();
					jQuery.ajax({
				        type: 'POST',
				        url: '<?php echo admin_url('admin-ajax.php');?>',
				        dataType: "html", // add data type
				        data: { action : '<?php echo $taxonomia->slug; ?>_ajax_posts' },
				        success: function(response) {
				            $('.posts-area').html(response); 
				        }
				    });
				});
			</script>


	<?php endwhile; endif;
	}
?>